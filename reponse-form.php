<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Poppins:ital,wght@0,300;0,400;0,700;1,200&display=swap" rel="stylesheet"> 
    <title>Formulaire contact</title>
    </head>
    <body>
<?php   
if (empty($_POST)) : ?>
        <form action="reponse-form.php" method="post">
            <label>Prénom</label>
            <br />
            <input type="text" name="prenom" pattern="[A-Za-z-]{2,}" maxlength="50" required />
            <br />
            <label>Nom</label>
            <br />
            <input type="text" name="nom" pattern="[A-Za-z-]{2,}" maxlength="50" required />
            <br />
            <label>Entreprise</label>
            <br />
            <input type="text" name="entreprise" pattern="[A-Za-z0-9- ]{2,}" maxlength="50" />
            <br />
            <label>Fonction dans l'entreprise</label>
            <br />
            <input type="text" name="fonction" pattern="[A-Za-z ]{2,}" maxlength="50" />
            <br />
            <label>Téléphone</label>
            <br />
            <input type="tel" name="telephone" pattern="[0]{1}[1-7]{1}[0-9]{8}" />
            <br />
            <label>E-mail</label>
            <br />
            <input
            type="email"
            name="mail"
            pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
            minlength="7"
            required
            />
            <br />
            <label>Objet du message</label>
            <br />
            <input type="text" name="sujet" pattern="[A-Za-z0-9]{2,}" maxlength="50" required />
            <br />
            <label>Message</label>
            <br />
            <textarea
            name="message"
            rows="5"
            cols="33"
            pattern="[A-Za-z]{2,}"
            maxlength="1000"
            required
            ></textarea>
            <br />
            <input type="submit" value="Envoyer" />
        </form>
    </body>
</html>

<?php else :
//Comment valider les données en provenance de mon formulaire ?

//1 Les données obligatoires sont elles présentes ?
if( isset( $_POST["mail"]) && isset( $_POST["message"]) && isset( $_POST["prenom"]) && isset( $_POST["nom"]) && isset( $_POST["sujet"])){

    //2 Les données obligatoires sont-elles remplies ?
    if( !empty( $_POST["mail"]) && !empty( $_POST["message"]) && !empty( $_POST["prenom"]) && !empty( $_POST["nom"]) && !empty( $_POST["sujet"])){

        //3 Les données obligatoires ont-elles une valeur conforme à la forme attendue ?
        if( filter_var($_POST["mail"], FILTER_SANITIZE_EMAIL)){

            $_POST["mail"] = filter_var($_POST["mail"], FILTER_SANITIZE_EMAIL);

            if( filter_var($_POST["message"], FILTER_SANITIZE_FULL_SPECIAL_CHARS)){
                $_POST["message"] = filter_var($_POST["message"], FILTER_SANITIZE_FULL_SPECIAL_CHARS);

                if( filter_var($_POST["prenom"], FILTER_SANITIZE_STRING) && filter_var($_POST["nom"], FILTER_SANITIZE_STRING) && filter_var($_POST["sujet"], FILTER_SANITIZE_STRING)) {
                    $_POST["prenom"] = filter_var($_POST["prenom"], FILTER_SANITIZE_STRING);
                    $_POST["nom"] = filter_var($_POST["nom"], FILTER_SANITIZE_STRING);
                    $_POST["sujet"] = filter_var($_POST["sujet"], FILTER_SANITIZE_STRING);
    
                    //4 Les données optionnelles sont-elles présentes ? Les données optionnelles sont-elles remplies ? Les données optionnelles ont-elles une valeur conforme à la forme attendue ?                
                    if( isset($_POST["entreprise"])){
                        $_POST["entreprise"] = filter_var($_POST["entreprise"], FILTER_SANITIZE_STRING);
                    }else{$_POST["entreprise"] = "";
                    }

                    if( isset($_POST["fonction"])){
                        $_POST["fonction"] = filter_var($_POST["fonction"], FILTER_SANITIZE_STRING);
                    }else{$_POST["fonction"] = "";
                    }

                    if( isset($_POST["telephone"])){
                        $_POST["telephone"] = filter_var($_POST["telephone"], FILTER_SANITIZE_STRING);
                    }else{$_POST["telephone"] = "";
                    }

                    //5 afficher le mail et le message
?>
                    <h2>Bonjour <?php printf('%s', $_POST["prenom"]); ?>, votre message a été envoyé et vous serez contacté dans les plus bref délais !</h2>
                    <meta http-equiv="refresh" content="1;URL=reponse-form.php"> 
<?php
//6 utiliser la fonction mail() pour envoyer le message vers botre boîte mail
                    $to = "savinien.monteil@gmail.com";
                    $subject = $_POST["sujet"];
                    $message = 'Vous avez un nouveau message de ' . $_POST["prenom"] . ' ' . $_POST["nom"];
                    $message .= PHP_EOL . 'Contenu du message : ' . $_POST["message"];
                    if( $_POST["entreprise"]){
                        $message .= PHP_EOL . 'Entreprise : ' . $_POST["entreprise"];}
                    if( $_POST["fonction"]){
                        $message .= PHP_EOL . 'Fonction dans l\'entreprise : ' . $_POST["fonction"];}
                    if( $_POST["telephone"]){
                        $message .= PHP_EOL . 'Téléphone : ' . $_POST["telephone"];}
                    $headers = 'From: ' . $_POST["mail"]. PHP_EOL .
                    'Reply-To: ' .$_POST["mail"]. PHP_EOL .
                    'Content-Type: text/html; charset=UTF-8'. PHP_EOL .
                    'X-Mailer: PHP/' . phpversion() ;
                    mail($to, $subject, $message, $headers);


                }else {print("Fournissez des informations valides svp !");
                }

            }else {print("La forme de votre message a outrepassé toutes nos prédictions !");
            }

        }else {print("Saisir une adresse email valide !");
        }

    }else {print("Il faut saisir les valeurs obligatoires !");
    }
    
    }else {print("RENDS-MOI MES CODES PETIT DÉGLINGO !");
    }
?>
<a href="#" onclick="history.go(-1);">Retour</a>
<?php
endif;
